# 使用指南



## 建立`个人作业`公开主页



## Fork 模板

打开  [101Camp / write / works · GitLab](https://gitlab.com/101camp/write/works)  页面，点右上角 `Fork` 按键。

![msPGqK.jpg](https://s2.ax1x.com/2019/08/23/msPGqK.jpg)



## 改名字

左侧栏进入 Settings > General 

找到 Naming, topics, avatar

将 Project name 改成 `username.gitlab.io`   ( username 是你的 gitlab id )，例如我的 id 是 cnfeat，我就修改成 `cnfeat.gitlab.io`  

![etQra6.jpg](https://s2.ax1x.com/2019/07/31/etQra6.jpg)

## 运行

左侧栏进入 CI/CD ，点击 `Run Pipeline`

点击下一步，完成余下操作，页面的 `Stages` 会显示在运行

![etQsIK.jpg](https://s2.ax1x.com/2019/07/31/etQsIK.jpg)

待运行图标变成绿色勾状图标，即表示页面已经部署完毕

## 完成

此时，打开 `username.gitlab.io`

就会显示你的个人作业主页。

示例：[Introduction · GitBook](https://cnfeat.gitlab.io/)

## 提交作业

进入作业仓库：[Files · master · 101Camp / write / works · GitLab](https://gitlab.com/101camp/write/works/tree/master)

进入本周作业文件夹：[1w · master · 101Camp / write / works · GitLab](https://gitlab.com/101camp/write/works/tree/master/1w) （以第一周为例）

`card&weekly.md` 是你提交每日/周卡片的地方

`work.md` 是你提交每周作业的地方

点击右上角的 `Web IDE` 即可进入 `网页编辑器` 模式

![msk3Mn.jpg](https://s2.ax1x.com/2019/08/23/msk3Mn.jpg)

选择 `card&weekly.md` 即可编辑。

![msk8rq.jpg](https://s2.ax1x.com/2019/08/23/msk8rq.jpg)



编辑完成之后，点击左下角的 `commit`

![msAqn1.jpg](https://s2.ax1x.com/2019/08/23/msAqn1.jpg)




再点击 `Stage & Commit` 即提交成功

![msAL0x.jpg](https://s2.ax1x.com/2019/08/23/msAL0x.jpg)

你可以在 [Pipelines · 101Camp / write / works · GitLab](https://gitlab.com/101camp/write/works/pipelines) 看到你的提交记录和进度。

![msAHXR.jpg](https://s2.ax1x.com/2019/08/23/msAHXR.jpg)

当页面的 `Stages` 图标变成绿色勾状图标，即表示页面已经部署完毕

你可以在 `username.gitlab.io` 看到你的提交更新。

## 探索任务

如何上传卡片图片？请自由摸索，写出你的解决办法。



### Changelog

- 2019-08-23 cnfeat 创建

